<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIndexToRemittanceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('remittance', function (Blueprint $table) {
            $table->index('source_user_id', 'ind_source_user_id');
            $table->index('destination_user_id', 'ind_destination_user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('remittance', function (Blueprint $table) {
            $table->dropIndex('ind_source_user_id');
            $table->dropIndex('ind_destination_user_id');
        });
    }
}
