<?php

use App\Remittance;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

/**
 * Seed для заполнения таблицы переводов большим колиеством случайных значений
 */
class MakeRemittanceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::disableQueryLog();

        Remittance::truncate();
        $maxRecords = 100000;

        DB::beginTransaction();
        for ($i = 1; $i++ <= $maxRecords;) {
            $sourceUserId = $this->getRand();
            $remittance = new Remittance;
            $remittance->setSourceUserId($sourceUserId);
            $remittance->setDestinationUserId($this->getDestinationUserId($sourceUserId));
            $remittance->setAmount($this->getAmountRand());
            $remittance->setRemittanceTime($this->getDateRand());
            
            if ($i % 10000 === 0) {
                DB::commit();
                DB::beginTransaction();
            }
            
            $remittance->save();
        }
        DB::commit();
    }
    
    private function getDestinationUserId($sourceUserId)
    {
        $destinationId = $this->getRand();
        
        return $destinationId == $sourceUserId ? $destinationId + 1: $destinationId;
    }
    
    private function getRand()
    {
        return mt_rand(1, 7);
    }
    
    private function getAmountRand()
    {
        return mt_rand(10, 10000);
    }
    
    private function getDateRand()
    {
        $hour = mt_rand(0, 23);
        $day = mt_rand(1, 4);
        
        $date = sprintf(
            "2020-01-%.02s %s:00:00",
            $day,
            $hour
        );
        
        return new DateTime($date);
    }
}
