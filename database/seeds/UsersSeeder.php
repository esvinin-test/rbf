<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $names = [
            'Карпов Агафон' => '100000',
            'Попов Августин' => '20000',
            'Соколов Аристарх' => '150000',
            'Матвеев Лев' => '30000',
            'Гущин Денис' => '100000',
            'Егоров Виссарион' => '100000',
            'Пестов Борис' => '230000',
            'Шашков Лука' => '33000',
        ];
        
        foreach ($names as $name => $balance) {
            \Illuminate\Support\Facades\DB::table('users')->insert([
                'name' => $name,
                'balance' => $balance,
            ]);
        }
    }
}
