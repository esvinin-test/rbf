@extends('layouts.main')


@section('content')
<h1>Список пользователей</h1>

@include('errors')

@if (\Session::has('success'))
    <div class="alert alert-success">
        {{ \Session::get('success') }}
    </div>
@endif

<table class="table table-striped table-condensed table-hover table-responsive">
    <tr>
        <th>Пользователь</th>
        <th>Управление</th>
    </tr>
    
<?php foreach($users as $user) :?>
    <tr>
        <td><?=$user->name?></td>
        <td>
            @if (Auth::check())
                <a href='<?=url("/remittance/{$user->id}")?>'> Перевести  </a>
            @else
                <a href='<?=url("/auth/login/{$user->id}")?>'> Залогиниться </a>
            @endif
        </td>
    </tr>
<?php endforeach ?>
</table>

<?=$users->links()?>
@endsection