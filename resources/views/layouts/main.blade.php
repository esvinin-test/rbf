<html>
    <head>
        <link rel="stylesheet" type="text/css" href="/bootstrap/css/bootstrap.css">
        <script type="text/javascript" src="/bootstrap/js/bootstrap.js"></script>	
    </head>
    <body>
                <style>
.navbar-custom {
    background-color: #139fb5;
    background-image: linear-gradient(to bottom, #18c6e1 0%, #118ea2 100%);
    color: #ffffff;
    border-radius: 0;
}
        </style>
        <!-- Fixed navbar -->
    <nav class="navbar navbar-default navbar-custom navbar-fixed-top" role="navigation">
        <div class="container">
            <div style="float: right; height: 100%;">
            @auth
                <div>
                    #{{ Auth::user()->id }} {{ Auth::user()->name }}  <a style="color:white" href="{{url('/auth/logout')}}">[X]</a>
                </div>
                <div>
                    Ваш баланс: {{Auth::user()->getAvailableBalance()}}
                </div>
            @endauth
            @guest
                Необходимо выбрать пользвателя
            @endguest
                <div>
                    <a style="color:white" href="{{url('/remittance/stat')}}">Статистика переводов</a>
                </div>
            </div>
        </div>
    </nav>

        <div class="container">
            @yield('content')
        </div>
    </body>
</html>