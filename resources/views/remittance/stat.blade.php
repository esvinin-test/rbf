@extends('layouts.main')

@section('content')
<h1>Последние переводы пользователей</h1>


<table class="table table-striped table-condensed table-hover table-responsive">
    
    <tr>
        <th>Пользователь</th>
        <th>Сумма</th>
        <th>Дата создания</th>
        <th>Дата проведения</th>
        <th>Получатель</th>
    </tr>
    
    @foreach($usersData as $userData)
    <tr>
        <td>{{$userData->name}}</td>
        <td>{{$userData->amount}}</td>
        <td>{{$userData->created_at}}</td>
        <td>{{$userData->remittance_time}}</td>
        <td>{{$userData->dname}}</td>
    </tr>
    @endforeach
</table>

@endsection