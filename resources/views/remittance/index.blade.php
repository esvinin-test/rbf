@extends('layouts.main')

@section('content')
<h1>Создание перевода</h1>

@include('errors')

<form method="post">
    <table class="table table-striped table-condensed table-hover table-responsive">
        <tr>
            <th>Доступные средства:</th>
            <td>{{$sourceUserBalance}}</td>
        </tr>
        <tr>
            <th>Перевод назначен для: </th>
            <td>{{$destinationUser->name}}</td>
        </tr>
        <tr>
            <th>Дата перевода: </th>
            <td>
                <input type="date" name="data[date]" value="{{old('date')}}">
            </td>
            
        </tr>
        <tr>
            <th>Час перевода: </th>
            <td><input type="number" name="data[hour]" min="0" max="23" class="form-control" value="{{old('hour')}}"></td>
        </tr>
        <tr>
            <th>Сумма перевода: </th>
            <td><input type="number" name="data[amount]" max="{{$sourceUserBalance}}" min="1" class="form-control" value="{{old('amount')}}"></td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: right">
                <input type="submit" value="Отправить" class="form-control">
            </td>
        </tr>
    </table>
</form>

@endsection