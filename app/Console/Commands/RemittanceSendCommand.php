<?php

namespace App\Console\Commands;

use App\Remittance;
use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class RemittanceSendCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'remittance:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Отправка отложенных платежей';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->signature = sprintf(
            'remittance:send {--date=%s} {--hour=%s}',
            date('Y-m-d'),
            date('H')
        );
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $date = $this->option('date');
        $hour = $this->option('hour', 0);
        
        $startTime = date('Y-m-d H:i:s');
        
        $dateTime = sprintf("%s %'.02s:00:00", $date, $hour);
        
        $this->validate(['date' => $date, 'hour' => $hour,]);
        
        $remittances = Remittance::where('is_done', 0)
            ->where('remittance_time', $dateTime)
            ->orderBy('created_at')
            ->get();
        
        /** @var Remittance $remittance */
        foreach ($remittances as $remittance) {
            $sourceUser = null;
            $destinationUser = null;
            DB::beginTransaction();
            $debug = [];
            try {
                $sourceUser = $this->getUser($remittance->getSourceUserId());
                
                if ($sourceUser->balance < $remittance->getAmount()) {
                    throw new \Exception('Недостаточно средств на балансе');
                }
            
                // Фактически списание баланс
                $sourceUser->balance -= $remittance->getAmount();
                $sourceUser->save();
                
                $destinationUser = $this->getUser($remittance->getDestinationUserId());
                
                // Пополнение баланса
                $destinationUser->balance += $remittance->getAmount();
                $destinationUser->save();
                
                // Отмечаем, что перевод завершен
                $remittance->setIsDone();
                $remittance->save();
                
                DB::commit();
                $message = sprintf('Совершен перевод #%s %s -> %s на сумму %s', $remittance->id, $sourceUser->name, $destinationUser->name, $remittance->getAmount());
                $this->info($message);
            }
            catch (\Exception $e) {
                DB::rollBack();
                $message = sprintf(
                    'Ошибка перевода #%s %s -> %s на сумму %s: %s', 
                    $remittance->id, 
                    $sourceUser->name ?? '', 
                    $destinationUser->name ?? '', 
                    $remittance->getAmount(),
                    $e->getMessage()
                );
                $this->info($message);
            }
        }
        
        $endTime = date('Y-m-d H:i:s');
        $this->info("{$startTime} - {$endTime}");
    }
    
    private function validate($params)
    {
        $validator = Validator::make($params, [
            'date'   => 'required|date_format:Y-m-d',
            'hour'   => 'required|numeric|min:0|max:23',
        ]);
        
        if ($validator->fails()) {
            foreach ($validator->errors()->all() as $error) {
                $this->error($error);
            }
            exit();
        }
    }
    
    private function getUser($userId)
    {
        return User::findOrFail($userId);
    }
}
