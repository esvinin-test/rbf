<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Модель для работы с переводами
 */
class Remittance extends Model
{
    protected $table = 'remittance';

    /**
     * Возвращает id перевода
     */
    public function getId()
    {
        return $this->attributes['id'];
    }

    /**
     * Возвращает id профайла, который осуществляет перевод
     * @return int
     */
    public function getSourceUserId(): int
    {
        return $this->attributes['source_user_id'];
    }
    
    /**
     * Устанавливает id профайла, который осуществляет перевод
     * @param int $userId
     */
    public function setSourceUserId(int $userId)
    {
        $this->attributes['source_user_id'] = $userId;
    }

    /**
     * Возвращает id профайла, которому осуществляется перевод
     * @return int
     */
    public function getDestinationUserId(): int
    {
        return $this->attributes['destination_user_id'];
    }
    
    /**
     * Устанавливает id профайла, которому осуществляется перевод
     * @param int $userId
     */
    public function setDestinationUserId(int $userId)
    {
        $this->attributes['destination_user_id'] = $userId;
    }

    /**
     * Возвращает время перевода
     * @return \DateTime
     */
    public function getRemittanceTime(): \DateTime
    {
        return new DateTime($this->attributes['remittance_time']);
    }
    
    /**
     * Утснавливает время перевода
     * @param \DateTime $time
     */
    public function setRemittanceTime(\DateTime $time)
    {
        $this->attributes['remittance_time'] = $time;
    }
    
    /**
     * Возвращает сумму перевода
     * @return int
     */
    public function getAmount(): int
    {
        return $this->attributes['amount'];
    }
    
    /**
     * Возвращает сумму перевода
     * @param int $amount
     */
    public function setAmount(int $amount)
    {
        $this->attributes['amount'] = $amount;
    }
    
    public function setIsDone()
    {
        $this->attributes['is_done'] = 1;
    }
}
