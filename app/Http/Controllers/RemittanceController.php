<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RemittanceController extends Controller
{
    private $redirectAction = 'UsersController@index';
    
    public function index(Request $request, $destinationUserId)
    {
        $sourceUser = Auth::user();
        
        if (!$sourceUser) {
            return redirect()->action($this->redirectAction);
        }
        
        $destinationUser = User::findOrFail($destinationUserId);
        
        if ($sourceUser->id == $destinationUserId) {
            return redirect()
                ->action($this->redirectAction)
                ->withErrors(['Невозможно создать перевод самому себе']);
        }
        
        $params = [
            'sourceUser' => $sourceUser, 
            'destinationUser' => $destinationUser, 
            'sourceUserBalance' => $sourceUser->getAvailableBalance(),
        ];
        
        return view('remittance.index', $params);
    }
    
    public function add(Request $request, $destinationUserId)
    {
        $sourceUser = Auth::user();
        
        if (!$sourceUser) {
            return redirect()->action($this->redirectAction);
        }
        $newRemittanceData = $request->get('data');
        
        $validator = \Illuminate\Support\Facades\Validator::make($newRemittanceData, [
            'date'   => 'required|date_format:Y-m-d|after:today',
            'hour'   => 'required|numeric|min:0|max:23',
            'amount' => 'required|numeric|min:1|max:'. $sourceUser->getAvailableBalance(),
        ]);

        if ($validator->fails()) {
            return redirect('/remittance/'. $destinationUserId)
                ->withErrors($validator)
                ->withInput($newRemittanceData);
        }
        $destinationUser = User::findOrFail($destinationUserId);
        
        if ($sourceUser->id == $destinationUserId) {
            return redirect('/remittance/'. $destinationUserId)
                ->withErrors(['Невозможно создать перевод самому себе'])
                ->withInput($newRemittanceData);
        }
        
        
        $time = sprintf('%s %s:00:00', $newRemittanceData['date'], $newRemittanceData['hour']);
        
        $remittance = new \App\Remittance;
        $remittance->setAmount($newRemittanceData['amount']);
        $remittance->setDestinationUserId($destinationUser->id);
        $remittance->setSourceUserId($sourceUser->id);
        $remittance->setRemittanceTime(new \DateTime($time));

        $remittance->save();
        
        $successMessage = sprintf(
            'Перевод пользователю %s на сумму %s успешно создан',
            $destinationUser->name,
            $newRemittanceData['amount']
        );
        return redirect()->action($this->redirectAction)->with('success', $successMessage);
    }
    
    public function stat(Request $request)
    {
        $sql = 
'SELECT 
    usr.`name`,
    rem.`remittance_time`,
    rem.`created_at`,
    rem.`amount`,
    dusr.`name` as dname
FROM 
    users usr 
    LEFT JOIN (
        SELECT
            `remittance_time`,
            `created_at`,
            `amount`, 
            `source_user_id`,
            `destination_user_id`
        FROM 
            `remittance`
        WHERE 
            `id` in (
                SELECT max(id) FROM `remittance` GROUP BY `source_user_id`
            )
    ) as rem on usr.id = rem.source_user_id
    LEFT JOIN `users` dusr ON rem.`destination_user_id` = dusr.`id`
';
        $response = \Illuminate\Support\Facades\DB::select($sql);
        
        return view('remittance.stat', ['usersData' => $response] );
    }
}
