<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function login(Request $request, $userId) 
    {
        $selectedUser = User::findOrFail((int) $userId);
        
        Auth::login($selectedUser);
        
        return redirect()->action('UsersController@index');
    }
    
    
    public function logout()
    {
        Auth::logout();
        
        return redirect()->action('UsersController@index');
    }
}
