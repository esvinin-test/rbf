<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UsersController extends Controller
{
    public function index(Request $request) 
    {
        $authorizedUserId = Auth::user()->id ?? 0;
        
        return view('user.profiles', ['users' => User::where('id', '!=', $authorizedUserId)->paginate(5)] );
    }
}
