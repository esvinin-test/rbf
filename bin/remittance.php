<?php

use Symfony\Component\Console\Application;
use Illuminate\Database\Capsule\Manager as Capsule;

require_once __DIR__ . '/../vendor/autoload.php';
$app = require_once __DIR__.'/../bootstrap/app.php';

$capsule = new Capsule;
$capsule->addConnection($app->config->get('database'));
$capsule->setAsGlobal();
$capsule->bootEloquent();

$console = new Application('Additional commands');

$console->addCommands([
    new App\Console\Commands\RemittanceSendCommand,
]);

$console->run();