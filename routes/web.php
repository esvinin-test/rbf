<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/auth/login/{userId}', '\App\Http\Controllers\AuthController@login')
    ->where('userId', '[0-9]+');
Route::get('/auth/logout', '\App\Http\Controllers\AuthController@logout');
Route::get('/users', '\App\Http\Controllers\UsersController@index');
Route::get('/remittance/{userId}', '\App\Http\Controllers\RemittanceController@index')
    ->where('userId', '[0-9]+');
Route::post('/remittance/{userId}', '\App\Http\Controllers\RemittanceController@add')
    ->where('userId', '[0-9]+');

Route::get('/remittance/stat', '\App\Http\Controllers\RemittanceController@stat');